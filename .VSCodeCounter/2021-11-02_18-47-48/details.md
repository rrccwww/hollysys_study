# Details

Date : 2021-11-02 18:47:48

Directory e:\rcw\HollyStudy\second_month\vue-cli\vue_test

Total : 39 files,  19998 codes, 247 comments, 306 blanks, all 20551 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.eslintrc.js](/.eslintrc.js) | JavaScript | 20 | 9 | 2 | 31 |
| [.postcssrc.js](/.postcssrc.js) | JavaScript | 7 | 2 | 2 | 11 |
| [README.md](/README.md) | Markdown | 20 | 0 | 11 | 31 |
| [build/build.js](/build/build.js) | JavaScript | 35 | 0 | 7 | 42 |
| [build/check-versions.js](/build/check-versions.js) | JavaScript | 45 | 0 | 10 | 55 |
| [build/utils.js](/build/utils.js) | JavaScript | 80 | 5 | 17 | 102 |
| [build/vue-loader.conf.js](/build/vue-loader.conf.js) | JavaScript | 21 | 0 | 2 | 23 |
| [build/webpack.base.conf.js](/build/webpack.base.conf.js) | JavaScript | 85 | 4 | 4 | 93 |
| [build/webpack.dev.conf.js](/build/webpack.dev.conf.js) | JavaScript | 82 | 7 | 7 | 96 |
| [build/webpack.prod.conf.js](/build/webpack.prod.conf.js) | JavaScript | 118 | 24 | 8 | 150 |
| [config/dev.env.js](/config/dev.env.js) | JavaScript | 6 | 0 | 3 | 9 |
| [config/index.js](/config/index.js) | JavaScript | 39 | 32 | 17 | 88 |
| [config/prod.env.js](/config/prod.env.js) | JavaScript | 5 | 0 | 2 | 7 |
| [config/test.env.js](/config/test.env.js) | JavaScript | 6 | 0 | 2 | 8 |
| [index.html](/index.html) | HTML | 11 | 1 | 1 | 13 |
| [package-lock.json](/package-lock.json) | JSON | 17,051 | 0 | 1 | 17,052 |
| [package.json](/package.json) | JSON | 90 | 0 | 1 | 91 |
| [src/OrganizationManage.vue](/src/OrganizationManage.vue) | Vue | 175 | 2 | 19 | 196 |
| [src/UserManage.vue](/src/UserManage.vue) | Vue | 163 | 1 | 19 | 183 |
| [src/assets/md5.js](/src/assets/md5.js) | JavaScript | 185 | 49 | 22 | 256 |
| [src/components/OrgsUser.vue](/src/components/OrgsUser.vue) | Vue | 234 | 9 | 15 | 258 |
| [src/components/Tabs.vue](/src/components/Tabs.vue) | Vue | 130 | 3 | 25 | 158 |
| [src/components/UserOrganTree.vue](/src/components/UserOrganTree.vue) | Vue | 53 | 1 | 13 | 67 |
| [src/components/bus.js](/src/components/bus.js) | JavaScript | 2 | 0 | 0 | 2 |
| [src/components/eject.vue](/src/components/eject.vue) | Vue | 174 | 2 | 12 | 188 |
| [src/components/orgsTable.vue](/src/components/orgsTable.vue) | Vue | 104 | 7 | 5 | 116 |
| [src/components/searchBar.vue](/src/components/searchBar.vue) | Vue | 61 | 2 | 5 | 68 |
| [src/components/treeTest.vue](/src/components/treeTest.vue) | Vue | 342 | 16 | 13 | 371 |
| [src/login.vue](/src/login.vue) | Vue | 109 | 3 | 10 | 122 |
| [src/main.js](/src/main.js) | JavaScript | 18 | 4 | 5 | 27 |
| [src/router/index.js](/src/router/index.js) | JavaScript | 33 | 0 | 8 | 41 |
| [src/test.vue](/src/test.vue) | Vue | 352 | 39 | 14 | 405 |
| [test/e2e/custom-assertions/elementCount.js](/test/e2e/custom-assertions/elementCount.js) | JavaScript | 18 | 8 | 2 | 28 |
| [test/e2e/nightwatch.conf.js](/test/e2e/nightwatch.conf.js) | JavaScript | 40 | 1 | 6 | 47 |
| [test/e2e/runner.js](/test/e2e/runner.js) | JavaScript | 33 | 8 | 8 | 49 |
| [test/e2e/specs/test.js](/test/e2e/specs/test.js) | JavaScript | 12 | 5 | 3 | 20 |
| [test/unit/jest.conf.js](/test/unit/jest.conf.js) | JavaScript | 27 | 3 | 1 | 31 |
| [test/unit/setup.js](/test/unit/setup.js) | JavaScript | 2 | 0 | 2 | 4 |
| [test/unit/specs/HelloWorld.spec.js](/test/unit/specs/HelloWorld.spec.js) | JavaScript | 10 | 0 | 2 | 12 |

[summary](results.md)
# Summary

Date : 2021-11-02 18:47:48

Directory e:\rcw\HollyStudy\second_month\vue-cli\vue_test

Total : 39 files,  19998 codes, 247 comments, 306 blanks, all 20551 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 2 | 17,141 | 0 | 2 | 17,143 |
| Vue | 11 | 1,897 | 85 | 150 | 2,132 |
| JavaScript | 24 | 929 | 161 | 142 | 1,232 |
| Markdown | 1 | 20 | 0 | 11 | 31 |
| HTML | 1 | 11 | 1 | 1 | 13 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 39 | 19,998 | 247 | 306 | 20,551 |
| build | 7 | 466 | 40 | 55 | 561 |
| config | 4 | 56 | 32 | 24 | 112 |
| src | 15 | 2,135 | 138 | 185 | 2,458 |
| src\assets | 1 | 185 | 49 | 22 | 256 |
| src\components | 8 | 1,100 | 40 | 88 | 1,228 |
| src\router | 1 | 33 | 0 | 8 | 41 |
| test | 7 | 142 | 25 | 24 | 191 |
| test\e2e | 4 | 103 | 22 | 19 | 144 |
| test\e2e\custom-assertions | 1 | 18 | 8 | 2 | 28 |
| test\e2e\specs | 1 | 12 | 5 | 3 | 20 |
| test\unit | 3 | 39 | 3 | 5 | 47 |
| test\unit\specs | 1 | 10 | 0 | 2 | 12 |

[details](details.md)
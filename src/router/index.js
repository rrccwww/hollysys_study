import Vue from 'vue'
import Router from 'vue-router'
import login from '@/login'
import UserManage from '@/UserManage'
import OrganizationManage from '@/OrganizationManage'
import test from '@/test'



import ViewUI from 'view-design'


Vue.use(Router);
Vue.use(ViewUI);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path:'/UserManage',
      name:'UserManage',
      component: UserManage
    },
    {
      path:'/OrganizationManage',
      name:'OrganizationManage',
      component: OrganizationManage
    },
    {
      path:'/test',
      name:'test',
      component:test
    }

  ]
  })

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios';
import ViewUI from 'view-design';
import 'view-design/dist/styles/iview.css';
Vue.config.productionTip = false;
Vue.prototype.$axios = axios
// Vue.prototype.HOST = '/api'
axios.defaults.headers['Content-Type'] = "application/x-www-form-urlencoded"

Vue.use(ViewUI);
Vue.use(VueAxios,axios);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  components: {},
  template: '<router-view/>'
})


